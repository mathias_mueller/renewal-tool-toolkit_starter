/**
 * 
 */
package de.infinigate.renewalTool.starter;

import javax.swing.JOptionPane;


/**
 * Class that serves as central entry-point into the Renewal-Toolkit.
 * Starts a different part of the toolkit depending on which parameters are passed.
 * @author mathias.mueller
 */
public class RenewalToolkitStarter {

	/**
	 * Main-Method.
	 * @param args the program to start. (1 = DataParser, 2 = DataEnricher, 3 = MailWizard)
	 */
	public static void main(String[] args) {
		
		if (args.length < 1) {
			JOptionPane.showMessageDialog(null, "The Renewal Toolkit Starter was not supplied with any arguments, so it does not know which Tool to start.", "No Tool specified", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} else {
			int toolId = Integer.parseInt(args[0]);
			switch (toolId) {
			case 1:
				de.infinigate.renewalTool.dataParser.gui.Wizard.main(null);
				break;
			case 2:
				// Placeholder for the DataEnricher
				JOptionPane.showMessageDialog(null, "The Renewal Toolkit Starter was requested to start the Renewal DataEnricher. Unfortunately this tool is not yet released.", "Unreleased Tool selected", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
				break;
			case 3:
				de.infinigate.renewalTool.mailWizard.gui.Wizard.main(null);
				break;
			default:
				JOptionPane.showMessageDialog(null, "The Renewal Toolkit Starter was supplied with an invalid argument, so it does not know which Tool to start.", "Invalid argument", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		}

	}

}
